import React from "react";

function Form() {
  return (
    <div className="p-3">
      <a
        href="/"
        className="d-flex align-items-center pb-3 mb-3 link-dark text-decoration-none border-bottom"
      >
        <span className="fs-5 fw-semibold text-primary">
          Find perfect home for you
        </span>
      </a>
      <form>
        <div className="mb-2">
          <label for="exampleInputEmail1" className="form-label">
            Form Option 1
          </label>
          <input
            type="email"
            className="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
          />
        </div>
        <div className="mb-2">
          <label for="exampleInputPassword1" className="form-label">
            Form Option 2
          </label>
          <input
            type="password"
            className="form-control"
            id="exampleInputPassword1"
          />
        </div>

        <div className="mb-2">
          <label for="exampleInputPassword1" className="form-label">
            Form Option 3
          </label>
          <input
            type="password"
            className="form-control"
            id="exampleInputPassword1"
          />
        </div>
        <div className="mb-2">
          <label for="exampleInputPassword1" className="form-label">
            Form Option 4
          </label>
          <input
            type="password"
            className="form-control"
            id="exampleInputPassword1"
          />
        </div>

        <div class="d-grid gap-2 d-md-flex justify-content-md-end mt-3">
          <button class="btn btn-primary me-md-2" type="button">
            Search
          </button>
        </div>
      </form>
    </div>
  );
}

export default Form;
