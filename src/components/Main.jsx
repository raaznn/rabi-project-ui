import React from "react";

//components
import Sidebar from "./Sidebar";
import Form from "./Form";
import Map from "./Map";
import Suggestions from "./Suggestions";

function Main() {
  return (
    <div className="d-flex container ">
      <Sidebar />

      <div className="bg-white container-sm border">
        <Form />
        <Map />
      </div>

      <Suggestions />
    </div>
  );
}

export default Main;
