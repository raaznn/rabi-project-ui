import React from "react";

function Map() {
  return (
    <div className="p-3">
      <img
        width="100%"
        src="https://fdn.gsmarena.com/imgroot/news/21/05/google-maps-updates/-1200/gsmarena_004.jpg"
      />

      <input
        type="range"
        className="form-range mt-3    "
        id="customRange1"
      ></input>

      <div className="d-grid gap-2 d-md-flex justify-content-md-end mt-3">
        <button className="btn btn-primary me-md-2" type="button">
          Search
        </button>
      </div>
    </div>
  );
}

export default Map;
