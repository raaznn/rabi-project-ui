import React from "react";

function Suggestions() {
  return (
    <div style={{ width: "550px" }} classNameName="flex-shrink bg-white p-3">
      <a
        href="/"
        className="d-flex align-items-center p-3 mb-3 link-dark text-decoration-none border-bottom"
      >
        <span className="fs-5 fw-semibold">Recently Added</span>
      </a>

      {/* CARD   */}
      <div className="card m-2" style={{ width: "" }}>
        <img
          src="https://www.rocketmortgage.com/resources-cmsassets/RocketMortgage.com/Article_Images/Large_Images/TypesOfHomes/types-of-homes-hero.jpg"
          className="card-img-top"
          alt="..."
        />
        <div className="card-body">
          <h5 className="card-title">House 1</h5>
          <a href="#" className="btn btn-primary btn-sm m-2">
            Click 1
          </a>
          <a href="#" className="btn btn-success btn-sm m-2">
            Click 2
          </a>
          <a href="#" className="btn btn-danger btn-sm m-2">
            Click 3
          </a>
        </div>
      </div>

      {/* CARD   */}
      <div className="card m-2" style={{ width: "" }}>
        <img
          src="https://images.pexels.com/photos/106399/pexels-photo-106399.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
          className="card-img-top"
          alt="..."
        />
        <div className="card-body">
          <h5 className="card-title">House 1</h5>
          <a href="#" className="btn btn-primary btn-sm m-2">
            Click 1
          </a>
          <a href="#" className="btn btn-success btn-sm m-2">
            Click 2
          </a>
          <a href="#" className="btn btn-danger btn-sm m-2">
            Click 3
          </a>
        </div>
      </div>

      {/* CARD   */}
      <div className="card m-2" style={{ width: "" }}>
        <img
          src="https://cdn.vox-cdn.com/thumbor/frFQQhOsxl8DctGjkR8OLHpdKMs=/0x0:3686x2073/1200x800/filters:focal(1549x743:2137x1331)/cdn.vox-cdn.com/uploads/chorus_image/image/68976842/House_Tour_Liverman_3D6A3138_tour.0.jpg"
          className="card-img-top"
          alt="..."
        />
        <div className="card-body">
          <h5 className="card-title">House 1</h5>
          <a href="#" className="btn btn-primary btn-sm m-2">
            Click 1
          </a>
          <a href="#" className="btn btn-success btn-sm m-2">
            Click 2
          </a>
          <a href="#" className="btn btn-danger btn-sm m-2">
            Click 3
          </a>
        </div>
      </div>
    </div>
  );
}

export default Suggestions;
